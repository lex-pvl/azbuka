$(document).ready(function() {
	$('.form-valid').each(function() {
		$(this).validate({
			rules: {
				first_name: {
					required: true
				},
				fullname: {
					required: true
				},
				last_name: {
					required: true
				},
				phone: {
					required: true,
				},
				email: {
					required: true,
					email: true
				},
				city: {
					required: true,
				},
				street: {
					required: true
				},
				house: {
					required: true
				},
				password: {
					required: true
				},
				repassword: {
					required: true,
					equalTo: '#password'
				}
			},
			messages: {
				first_name: {
					required: 'Поле обязательно для заполнения'
				},
				fullname: {
					required: 'Поле обязательно для заполнения'
				},
				last_name: {
					required: 'Поле обязательно для заполнения'
				},
				phone: {
					required: 'Номер введен неправильно*'
				},
				email: {
					required: 'Поле обязательно для заполнения',
					email: 'Введите корректный адрес'
				},
				city: {
					required: 'Поле обязательно для заполнения'
				},
				street: {
					required: 'Поле обязательно для заполнения'
				},
				house: {
					required: ''
				},
				password: {
					required: 'Поле обязательно для заполнения'
				},
				repassword: {
					required: 'Поле обязательно для заполнения',
					equalTo: 'Пароли не совпадают'
				}
			}
		})
	});
});