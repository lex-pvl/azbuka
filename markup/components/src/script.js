$(document).ready(function() {
	sliders();
	up();
	hoveredMenu();
	tabs();
	orderInProfile();
	addNewPhone();
	addNewAddress();
	replaceSymbols();
	selects();
	cardValue();
	initMap();
	productImages();
	showPopup();
	fixedHeader();
	comparisonHover();
	toggleFilters();
	leafAnimate();
	showMobileElems();
	phoneMask();
	media();
});

function sliders() {
	let mainSlider = new Swiper('.main-container', {
		slidesPerView: 1,
		spaceBetween: 20,
		pagination: {
			el: '.main-pagination',
		},
		lazy: true
	});

	let heatSlider = new Swiper('.heat-container', {
		slidesPerView: 6,
		spaceBetween: 20,
		navigation: {
			nextEl: '.heat-next',
			prevEl: '.heat-prev',
		},
		breakpoints: {
			320: {
				slidesPerView: 2,
				spaceBetween: 10,
			},
			550: {
				slidesPerView: 3,
			},
			768: {
				slidesPerView: 4,
			},
			992: {
				slidesPerView: 5,
			},
			1140: {
				slidesPerView: 5,
			},
			1450: {
				slidesPerView: 6
			}
		}
	});

	let actionSlider = new Swiper('.action-container', {
		slidesPerView: 6,
		spaceBetween: 20,
		navigation: {
			nextEl: '.action-next',
			prevEl: '.action-prev',
		},
		breakpoints: {
			320: {
				slidesPerView: 2,
				spaceBetween: 10,
			},
			550: {
				slidesPerView: 3,
			},
			768: {
				slidesPerView: 4,
			},
			992: {
				slidesPerView: 5,
			},
			1140: {
				slidesPerView: 5,
			},
			1450: {
				slidesPerView: 6
			}
		}
	});

	let lkSlider = new Swiper('.lk-container', {
		slidesPerView: 3,
		spaceBetween: 20,
		navigation: {
			nextEl: '.lk-next',
			prevEl: '.lk-prev',
		},
		breakpoints: {
			320: {
				slidesPerView: 2
			},
			768: {
				slidesPerView: 4,
			},
			992: {
				slidesPerView: 2,
			},
			1140: {
				slidesPerView: 2,
			},
			1450: {
				slidesPerView: 3
			}
		}
	});

	let comparisonSlider = new Swiper('.comparison-container', {
		slidesPerView: 5,
		spaceBetween: 0,
		pagination: {
			el: '.swiper-pagination',
			type: 'progressbar',
		},
		breakpoints: {
			320: {
				slidesPerView: 2
			},
			768: {
				slidesPerView: 3,
			},
			992: {
				slidesPerView: 4,
			},
			1140: {
				slidesPerView: 5,
			},
		}
	});

	// let recomendedSlider = new Swiper('.tovar-container', {
	// 	slidesPerView: 5,
	// 	spaceBetween: 20,
	// 	navigation: {
	// 		nextEl: '.tovar-next',
	// 		prevEl: '.tovar-prev',
	// 	},
	// 	breakpoints: {
	// 		320: {
	// 			slidesPerView: 2,
	// 		},
	// 		550: {
	// 			slidesPerView: 3,
	// 		},
	// 		768: {
	// 			slidesPerView: 4,
	// 		},
	// 		992: {
	// 			slidesPerView: 5,
	// 		},
	// 		1140: {
	// 			slidesPerView: 5,
	// 		},
	// 	}
	// });
}

function up() {
	$('#up').on('click', function(e){
		e.preventDefault();
		$('html,body').stop().animate({ scrollTop: 0 }, 1000);
	});
}

function parallax(e) {
	document.querySelectorAll('.list').forEach(layer => {
		let speed = layer.getAttribute('data-speed');
		let x = (e.clientX * speed) / 100;
		let y = (e.clientY * speed) / 100;

		layer.style.transform = `translate(${x}px, ${y}px)`;
		layer.style.OTransform = `translate(${x}px, ${y}px)`;          
		layer.style.msTransform = `translate(${x}px, ${y}px)`;         
		layer.style.MozTransform = `translate(${x}px, ${y}px)`;        
		layer.style.WebkitTransform = `translate(${x}px, ${y}px)`;
	});
}

document.addEventListener('mousemove', parallax);


function hoveredMenu() {
	let item = $('.menu-item');

	item.each(function() {
		$(this).hover(function() {
			$(this).addClass('active');
		}, function() {
			$(this).removeClass('active');
		});
	});

	let link = $('.nav-link');

	link.each(function() {
		$(this).hover(function() {
			$(this).addClass('active');
		}, function() {
			$(this).removeClass('active');
		});
	});
}


function tabs() {

	let recomendedSlider;

	$('.tovar-container').each(function() {
		let tn = $(this).find('.tovar-next');
		let tp = $(this).find('.tovar-prev');

		recomendedSlider = new Swiper($(this), {
			slidesPerView: 5,
			spaceBetween: 20,
			navigation: {
				nextEl: tn,
				prevEl: tp,
			},
			breakpoints: {
				320: {
					slidesPerView: 2,
				},
				550: {
					slidesPerView: 3,
				},
				768: {
					slidesPerView: 4,
				},
				992: {
					slidesPerView: 5,
				},
				1140: {
					slidesPerView: 5,
				},
			}
		});
	});

	$('ul.product__list').on('click', 'li:not(.active)', function() {
		$(this)
		.addClass('active').siblings().removeClass('active')
		.closest('.product').find('div.product__tab').removeClass('active').eq($(this).index()).addClass('active');
	});

	$('ul.tovar__list').on('click', 'li:not(.active)', function() {
		$(this)
		.addClass('active').siblings().removeClass('active')
		.closest('.product').find('div.tovar__tab').removeClass('active').eq($(this).index()).addClass('active');
		recomendedSlider.updateSize();
		recomendedSlider.updateSlides();
		recomendedSlider.updateProgress();
		recomendedSlider.updateSlidesClasses();
	});

	$('ul.order__list').on('click', 'li:not(.active)', function() {
		$(this).addClass('active').siblings().removeClass('active');
		$('#region').val($(this).data('region'));
		$('#city').val($(this).data('city'));
		$('#street').val($(this).data('street'));
		$('#house').val($(this).data('house'));
		$('#korpus').val($(this).data('korpus'));
		$('#str').val($(this).data('str'));
		$('#room').val($(this).data('room'));
	});
}

function orderInProfile() {
	let btn = $('.lk__body');

	btn.each(function() {
		$(this).on('click', function(event) {
			$(this).parents('.lk__item').find('.lk-opened').slideToggle(400);
			$(this).parents('.lk__item').toggleClass('active');
		});
	});
}

function addNewPhone() {
	let add = $('.phone-add');

	let remsvg = `<span class="remove">
	<svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 15 15">
	<path fill="#d90000" d="M.712 15.01l-.722-.722L6.778 7.5-.01.712.712-.01 7.5 6.777 14.288-.01l.722.722L8.222 7.5l6.788 6.788-.722.722L7.5 8.222z"/>
	</svg>
	</span>`


	add.each(function() {
		$(this).on('click', function() {
			
			var num = $('.lk-phone').length;
			console.log(num);
			var template = `
			<label class="lk__label lk-phone">
			<span>
			<span>Телефон</span>
			
			${remsvg}
			</span>
			<input name="phone[`+num+`][label]" type="hidden" class="lk__input lk-label" >
			<input name="phone[`+num+`][number]" type="text" class="lk__input" >	
			</label>`;
			
			if ( $(this).prev('.solo-phone').find('.remove').length === 0 ) {
				$(this).prev('.solo-phone').find('span').append(remsvg);
			}
			$(this).before(template);

			$('.ldata .remove').each(function() {
				$(this).on('click', function() {
					$(this).parents('.lk__label').remove();
				});
			});	
		});

		$('.ldata .remove').each(function() {
			$(this).on('click', function() {
				$(this).parents('.lk__label').remove();
			});
		});	
	});
}

function addNewAddress() {
	let add = $('.add-address');
	let rem = $('.remove');
	let edit = $('.address .edit')

	let remsvg = ` 
	<div class="remove">
	<svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 15 15">
	<path fill="#d90000" d="M.712 15.01l-.722-.722L6.778 7.5-.01.712.712-.01 7.5 6.777 14.288-.01l.722.722L8.222 7.5l6.788 6.788-.722.722L7.5 8.222z"/>
	</svg>
	</div>
	`


	add.each(function() {
		$(this).on('click', function(event) {
			
			var num = $('.address').length;
			
			var template = ` 
			<div class="lk__settings-item address">
			<div class="lk__settings-title">
			Адрес
			${remsvg}
			</div>
			<div class="lk__wrap">
			<label class="lk__label lk__address">
			<span>Регион</span>
			<input name="address[`+num+`][region]" type="text" class="lk__input">
			</label>
			<label class="lk__label lk__address">
			<span>Населенный пункт</span>
			<input name="address[`+num+`][city]" type="text" class="lk__input">
			</label>
			<label class="lk__label lk__address">
			<span>Улица</span>
			<input name="address[`+num+`][street]" type="text" class="lk__input">
			</label>
			</div>

			<div class="lk__home">
			<label class="lk__label lk__home-label">
			<span>Дом</span>
			<input name="address[`+num+`][house]" type="text" class="lk__input number">
			</label>
			<label class="lk__label lk__home-label">
			<span>Корпус</span>
			<input name="address[`+num+`][korpus]" type="text" class="lk__input number">
			</label>
			<label class="lk__label lk__home-label">
			<span>Строение</span>
			<input name="address[`+num+`][str]" type="text" class="lk__input number">
			</label>
			<label class="lk__label lk__home-label">
			<span>Квартира</span>
			<input name="address[`+num+`][room]" type="text" class="lk__input number">
			</label>
			<div class="add add-address">+ добавить еще один адрес</div>
			</div>
			`

			console.log(num);
			if ( $(this).parents('.address').find('.remove').length === 0 ) {
				$(this).parents('.address').find('.lk__settings-title').append(remsvg);
			}
			$(this).parents('.address').after(template);
			$(this).hide();
			replaceSymbols();

		});
	});		

	
	$(document).on('click', '.address .remove', function(event) {
		$(this).parents('.address').remove();
		if ( $('.address').length == 1) {
			$('.address').first().find('.add-address').show();
		}
	});
	

	$('.address .edit').each(function() {
		$(this).on('click', function() {
			$(this).parents('.address').find('.lk__input').first().focus();
		});
	});
}

function replaceSymbols() {
	var numberInput = $('.number');
	numberInput.each(function() {
		$(this).bind('change keyup input click', function() {
			if (this.value.match(/[^0-9]/g)) {
				this.value = this.value.replace(/[^0-9]/g, '');
			}
		});
	});
}

function selects() {
	let select = $('.select');

	select.each(function() {
		$(this).niceSelect();
	});
}

function cardValue() {
	var minus = $('.minus'),
	plus = $('.plus'),
	input = $('.val-input');

	input.each(function() {
		$(this).on('change input', function() {
			if ($(this).val() <= 1) {
				$(this).siblings('.minus').addClass('disabled').prop('disabled', true);
			} else {
				$(this).siblings('.minus').removeClass('disabled').prop('disabled', false);
			}
		});

		if ($(this).val() <= 1) {
			$(this).siblings('.minus').addClass('disabled').prop('disabled', true);
		} else {
			$(this).siblings('.minus').removeClass('disabled').prop('disabled', false);
		}
	});

	$(document).on('click', '.minus', function(){	
		event.preventDefault();
		var form = $(this).parent();
		var qty = +form.find('.val-input').val();
		qty = qty - 1;
		form.find('.val-input').val(qty);
		form.find('.btn-sm').submit();
		var price = String(form.find('.val-input').data('price'));
		price = Number(price.replace(' ', ''));
		form.parents('.cart__body').find('.sum').text((qty*price).toLocaleString('ru-RU'));

		if (qty <= 1) {
			form.find('.minus').addClass('disabled').prop('disabled', true);
		} else {
			form.find('.minus').removeClass('disabled').prop('disabled', false);
		}
	});

	$(document).on('click', '.plus', function(){	
		event.preventDefault();
		var form = $(this).parent();
		var qty = +form.find('.val-input').val();
		qty = qty + 1;
		form.find('.val-input').val(qty);
		form.find('.btn-sm').submit();
		var price = String(form.find('.val-input').data('price'));
		price = Number(price.replace(' ', ''));
		form.parents('.cart__body').find('.sum').text((qty*price).toLocaleString('ru-RU'));

		if (qty > 1) {
			form.find('.minus').removeClass('disabled').prop('disabled', false);
		} else {
			form.find('.minus').addClass('disabled').prop('disabled', true);
		}
	});
}

function initMap() {
	if ($('#map').is('#map')) {
		ymaps.ready(init);
		var iconUrl = $('#map').data('icon-url');
		var lat = $('#map').data('lat');
		var lng = $('#map').data('lng');
		var zoom = $('#map').data('zoom');

		function init() {
			var map = new ymaps.Map('map', {
				center: [lng, lat],
				zoom: zoom,
				controls: [],
				behaviors: ['drag']
			});

			var placemark = new ymaps.Placemark([lng, lat], {
				hintContent: ''
			},
			{
				iconLayout: 'default#image',
				iconImageHref: iconUrl,
				iconImageSize: [62, 74]
			});

			map.geoObjects.add(placemark)
		}
	}
}

function productImages() {
	let thumb = $('.product__thumb');
	let main = $('.product__big img');

	thumb.each(function() {
		let img = $(this).find('img').attr('src');
		$(this).on('click', function() {
			$(this).siblings().removeClass('active');
			$(this).addClass('active');
			main.attr('src', img);
		});
	});
}

function showPopup() {
	var popup = $('.popup');
	var overlay = $('.overlay');
	var width = $(window).width();
	var close = $('.popup__close');
	var popupOpen;
	var popupClose;

	popupOpen = function(popupName){
		$('body').css('width', width);
		$('body').addClass('no-scroll');
		popup.removeClass('active');
		$('.'+popupName).addClass('active');
		overlay.show();
	}
	popupClose = function(){
		popup.removeClass('active');
		overlay.hide();
		$('body').removeClass('no-scroll');
	}
	overlay.on('mousedown', function(){
		popupClose();
	});
	$(document).on('click', '.popup__close', function(){
		popupClose();
	});
	$(document).on('click', '.btn-close', function(){	
		popupClose();
	});
	$('body').on('click','[data-popup-open]',function(e){
		e.preventDefault();
		var popupName = $(this).data('popup-open');
		popupOpen(popupName);
	});
}

function fixedHeader() {
	const headHeight = $('.header').height();

	const nav = $('.fixed-nav');

	$('.header__bottom').clone().appendTo(nav);


	$(window).on('scroll', function() {
		if ($(window).scrollTop() > headHeight) {
			$('.fixed-header').addClass('active')
		} else {
			$('.fixed-header').removeClass('active')
		}
	});

	hoveredMenu();
}


function comparisonHover() {
	let elem = $('.comparison__elem-item');
	elem.each(function() {
		$(this).hover(function() {
			let q = $(this).index();
			$(this).addClass('active');
			$(this).parents('.comparison__elem').siblings().find(`.comparison__elem-item:eq(${q})`).addClass('active');
		}, function() {
			let q = $(this).index();
			$(this).removeClass('active');
			$(this).parents('.comparison__elem').siblings().find(`.comparison__elem-item:eq(${q})`).removeClass('active');
		});
	});
}

function toggleFilters(){
	let title = $('.filters__title');
	title.each(function() {
		$(this).on('click', function() {
			$(this).parent().toggleClass('active');
			$(this).next().slideToggle(250);
		});
	});
};

function leafAnimate() {
	let leaf = $('.leaf');

	$(window).on('load scroll', function(event) {
		if ($(window).scrollTop() > 400) {
			leaf.addClass('active');
		} else {
			leaf.removeClass('active');
		}
	});
}

function showMobileElems() {
	let searchBtn = $('.mobile-menu-search'),
	block = $('.mobile--search');

	searchBtn.on('click', function(event) {
		block.slideToggle(250);
	});

	let burger = $('.header__burger'),
	toggleMenu = $('.toggleMenu');

	burger.on('click', function() {
		$('body').toggleClass('no-scroll');
		$(this).toggleClass('active');
		toggleMenu.toggleClass('active');
	});
}

function media() {
	let wdt = $(window).width();

	if (wdt <= 767) {
		animateNavigationInMenu();
		filtersInMobile();

		let instaSlider = new Swiper('.insta-container', {
			slidesPerView: 2,
			spaceBetween: 0,
			slidesPerColumn: 2,
			navigation: {
				nextEl: '.insta-next',
				prevEl: '.insta-prev',
			},
		});

		let advantSlider = new Swiper('.advant-container', {
			slidesPerView: 2,
			spaceBetween: 0,
			pagination: {
				el: '.advant-pagination'
			},
			breakpoints: {
				320: {
					slidesPerView: 1
				},
				480: {
					slidesPerView: 2,
				}
			}
		})
	}
	$(window).on('load resize', function(event) {
		$('.instagramm__item').height($('.instagramm__item').width());
	});
}

function animateNavigationInMenu() {
	let item = $('.toggleMenu__nav .menu-item span');
	let back = $('.back-to-nav');

	item.on('click', function(event) {
		if ( $(this).parent().find('.hidden-menu').length !== 0 ) {
			$(this).parents('.toggleMenu__nav').addClass('transformed');
			$(this).siblings('.hidden-menu').addClass('active');
			$(this).siblings('.hidden-menu').addClass('transformed-plus');
		}
	});

	back.on('click', function(event) {
		$(this).parents('.toggleMenu__nav').removeClass('transformed');
		$(this).parent('.hidden-menu').removeClass('active');
		$(this).parent('.hidden-menu').removeClass('transformed-plus');
	});


	let navlink = $('.toggleMenu__subnav .nav-link');
	navlink.on('click', function() {
		$(this).toggleClass('opened');
		$(this).find('.nav-list').slideToggle();
	});


	let mItem = $('.mobile-menu-item');
	mItem.each(function() {
		$(this).find('.mobile-menu-span').on('click', function() {
			$(this).parents().siblings('.mobile-menu-item').find('.hidden-menu').removeClass('active');
			$(this).next('.hidden-menu').toggleClass('active');
		});

		let letter = $(this).find('.letter');
		letter.on('click', function(event) {
			$(this).parents('.alphabet').addClass('transformed');
			$(this).next('.alphabet-links').show();
		});

		let backToLetter = $(this).find('.back-to-letter');
		backToLetter.on('click', function() {
			$(this).parent('.alphabet-links').hide();
			$(this).parents('.alphabet').removeClass('transformed');
		});


		let nextSpan = $(this).find('.next--span');
		nextSpan.on('click', function(event) {
			event.preventDefault();
			$(this).parents('.elems').addClass('transformed');
			$(this).parent().next('.elem-items').show();
		});

		let elemBack = $(this).find('.elem--back');
		elemBack.on('click', function() {
			$(this).parent('.elem-items').hide();
			$(this).parents('.elems').removeClass('transformed');
		});

	});
};

function filtersInMobile() {
	let name = $('.catalog__name');
	let item = $('.filters__item');

	name.on('click', function(event) {
		$(this).toggleClass('active');
		$(this).nextAll().slideToggle(250);
	});
}

function phoneMask() {
	$('.phone-mask').each(function() {
		$(this).mask("+7 (999) 999-99-99");
	});
}
