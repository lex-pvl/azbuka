$(document).ready(function() {
	var menuLinks = `
	<a style="color: #fff; padding: 5px;" href="index.html">Главная</a>
	<a style="color: #fff; padding: 5px;" href="catalog.html">Каталог</a>
	<a style="color: #fff; padding: 5px;" href="result.html">Результаты поиска</a>
	<a style="color: #fff; padding: 5px;" href="news.html">Новости</a>
	<a style="color: #fff; padding: 5px;" href="fav.html">Избранное</a>
	<a style="color: #fff; padding: 5px;" href="articles.html">Статьи</a>
	<a style="color: #fff; padding: 5px;" href="lk-order.html">Личный кабинет</a>
	<a style="color: #fff; padding: 5px;" href="lk-settings.html">Личный кабинет</a>
	<a style="color: #fff; padding: 5px;" href="finish.html">Заказ принят</a>
	<a style="color: #fff; padding: 5px;" href="cart.html">Корзина</a>
	<a style="color: #fff; padding: 5px;" href="contacts.html">Контакты</a>
	<a style="color: #fff; padding: 5px;" href="comparison.html">Страница сравнения</a>
	<a style="color: #fff; padding: 5px;" href="about.html">О нас</a>
	<a style="color: #fff; padding: 5px;" href="product.html">Страница продукта</a>
	<a style="color: #fff; padding: 5px;" href="order.html">Страница оформления заказа - зарегистрировались</a>
	<a style="color: #fff; padding: 5px;" href="order2.html">Страница оформления заказа - не зарегистрировались</a>
	<a style="color: #fff; padding: 5px;" href="#" data-popup-open="popup__join">Модальное окно - Войти</a>
	<a style="color: #fff; padding: 5px;" href="#" data-popup-open="popup__register">Модальное окно - Регистрация</a>
	<a style="color: #fff; padding: 5px;" href="#" data-popup-open="popup__call">Модальное окно - Заказ обратного звонка</a>
	<a style="color: #fff; padding: 5px;" href="#" data-popup-open="popup__success">Модальное окно - Вы успешно зарегистрировались!</a>
	<a style="color: #fff; padding: 5px;" href="#" data-popup-open="popup__callback">Модальное окно - Заявка на обратный звонок принята</a>
	<a style="color: #fff; padding: 5px;" href="#" data-popup-open="popup__size">Модальное окно - Выберите размер</a>
	<a style="color: #fff; padding: 5px;" href="#" data-popup-open="popup__phone">Модальное окно - Введите название телефона</a>
	<a style="color: #fff; padding: 5px;" href="#" data-popup-open="popup__order">Модальное окно - Ваш заказ принят!</a>
	<a style="color: #fff; padding: 5px;" href="#" data-popup-open="popup__product">Модальное окно - Дерево Бонсай</a>
	<a style="color: #fff; padding: 5px;" href="#" data-popup-open="popup__product2">Модальное окно - Дерево Бонсай 2</a>
	<a style="color: #fff; padding: 5px;" href="#" data-popup-open="popup__product3">Модальное окно - Дерево Бонсай 3</a>
	`;


	var styleNavdd = 'width: 40px;height: 40px;border-radius: 50%;right: 5px;bottom: 5px;background-color: rgba(133,143,159, 0.6);position: fixed;color: #fff;display: -webkit-box;display: -ms-flexbox;display: flex;-webkit-box-align: center;-ms-flex-align: center;align-items: center;-webkit-box-pack: center;-ms-flex-pack: center;justify-content: center;-webkit-transition: 0.4s;-o-transition: 0.4s;transition: 0.4s;z-index: 300;';
	var styleNavddActive = 'width: 100%;height: 100%;border-radius: 0px;right: 0px;bottom: 0px;background-color: rgba(133,143,159, 0.95);position: fixed;color: #fff;display: -webkit-box;display: -ms-flexbox;display: flex;-webkit-box-align: center;-ms-flex-align: center; align-items: center;-webkit-box-pack: center;-ms-flex-pack: center;justify-content: center;-webkit-transition: 0.4s;-o-transition: 0.4s;transition: 0.4s;z-index: 300; overflow-y: scroll;';
	var styleNavdd_span = 'display: block;';
	var styleNavdd_spanActive = 'display: none;';
	var styleNavdd_div = 'pointer-events: none;flex-wrap: wrap;opacity: 0;display: -webkit-box;display: -ms-flexbox;display: flex;-webkit-box-orient: vertical;-webkit-box-direction: normal;-ms-flex-direction: column;flex-direction: column;position: absolute;overflow-x: hidden;overflow-y: auto;';
	var styleNavdd_divActive = 'pointer-events: auto;opacity: 1;flex-wrap: wrap;width: 300px;height: 100%;position: static;display: -webkit-box;display: -ms-flexbox;display: flex;-webkit-box-orient: vertical;-webkit-box-direction: normal;-ms-flex-direction: column;flex-direction: column;overflow-x: hidden;overflow-y: auto;';
	$('body').append('<div class="nav-d-d"><span>nav</span><div>'+menuLinks+'</div></div>');
	$('.nav-d-d').attr('style', styleNavdd).find('span').attr('style', styleNavdd_span).siblings('div').attr('style', styleNavdd_div);
	$('.nav-d-d').on('click',function(){
		if($(this).is('.active')){
			$(this).removeClass('active');
			$('.nav-d-d').attr('style', styleNavdd).find('span').attr('style', styleNavdd_span).siblings('div').attr('style', styleNavdd_div);
		}else{
			$(this).addClass('active');
			$('.nav-d-d').attr('style', styleNavddActive).find('span').attr('style', styleNavdd_spanActive).siblings('div').attr('style', styleNavdd_divActive);
		}
	})
});